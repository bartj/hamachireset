#include "StdAfx.h"
#include "HamachiResetCore.h"

void ThrowError(string error, int value = GetLastError())
{
	stringstream msg;
	msg << error << ": " << value;
	throw runtime_error(msg.str());
}

wstring GetExpandedPath(wstring const & original)
{
	vector<wchar_t> output(35000);
	if(ExpandEnvironmentStrings(original.c_str(), &output[0], output.size()) == 0)
		ThrowError("Failed to expand environment variable.");
	return &output[0];
}

class Redirect
{
private:
	PVOID oldValue;
	bool is64BitOs;

public:
	Redirect()
	{
		is64BitOs = (GetProcAddress(GetModuleHandle(L"kernel32"), "Wow64DisableWow64FsRedirection") != 0);

		if(is64BitOs)
			Wow64DisableWow64FsRedirection(&oldValue);
	}

	~Redirect()
	{
		if(is64BitOs)
			Wow64RevertWow64FsRedirection(oldValue);
	}
};

//must use cmd.exe so that it works on both 64 bit and 32 bit machines. 
//32 bit processes cannot access the System32 folder.
void DeleteFolder(wstring const & folder)
{
	Redirect redirect;

	wstringstream args;
	args << L"/c del /q \"" << folder << L"\\*\"";
	wstring argStringPerm = args.str();
	vector<wchar_t> argVector(begin(argStringPerm), end(argStringPerm));
	argVector.push_back(0);
	STARTUPINFO startupInfo = {0};
	PROCESS_INFORMATION procInfo = {0};
	wstring commandInterpreter = GetExpandedPath(L"%ComSpec%");
	if(!CreateProcess(commandInterpreter.c_str(), &argVector[0], nullptr, nullptr, FALSE, CREATE_NO_WINDOW, nullptr, nullptr, &startupInfo, &procInfo))
		ThrowError("Failed to launch cmd.exe.");
	CloseHandle(startupInfo.hStdInput);
	CloseHandle(startupInfo.hStdOutput);
	CloseHandle(startupInfo.hStdError);
	CloseHandle(procInfo.hProcess);
	CloseHandle(procInfo.hThread);
}

void HamachiResetCore::Reset()
{
	wstring paths[] = 
	{
		L"%USERPROFILE%\\..\\LocalService\\Local Settings\\Application Data\\LogMeIn Hamachi",
		L"%USERPROFILE%\\Local Settings\\Application Data\\LogMeIn Hamachi",
		L"%WINDIR%\\System32\\Config\\SystemProfile\\AppData\\Local\\LogMeIn Hamachi"
	};

	for_each(begin(paths), end(paths), [] (wstring path) {
		try
		{
			OutputDebugString((path + L"\n").c_str());
			wstring fullPath = GetExpandedPath(path);
			OutputDebugString((fullPath + L"\n").c_str());
			DeleteFolder(fullPath);
		}
		catch(runtime_error &e)
		{
			OutputDebugStringA(e.what());
		}
	});
}